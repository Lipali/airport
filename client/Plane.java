package client;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.util.Random;
import server.CommunicationProtocol;

import static java.lang.Thread.sleep;
import static server.CommunicationProtocol.ValueType.*;

public class Plane extends Thread{


    private String name ;
    private Point startPoint = new Point();
    PlaneRoad planeRoad ;


    public Plane (String name) throws IOException {
        this.name = name ;
        this.planeRoad= new PlaneRoad(startPoint);
    }
    public String getPlaneName () {return name;}
    public PlaneRoad getPlaneRoad(){
        return planeRoad;
    }

    Socket socket = new Socket("localhost",5051);
    DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
    DataInputStream din = new DataInputStream(socket.getInputStream());
    Gson gson = new Gson();
    JsonObject objectGson = new JsonObject();
    CommunicationProtocol communicationProtocol = new CommunicationProtocol();

    @Override
    public void run()  {
        int counter =0;
        while (true){

            try {
                Point actualPoint = getPlaneRoad().getPoint(counter);
                dout.writeUTF(gson.toJson(getPlaneName()));
                dout.writeUTF(gson.toJson(actualPoint));

                String jsonProtocol = din.readUTF();
                communicationProtocol = gson.fromJson(jsonProtocol,CommunicationProtocol.class);

                if (STOP.equals(communicationProtocol)) {
                }
                else if (START.equals(communicationProtocol)) {
                    counter++;
                }
                else if (FLY.equals(communicationProtocol)) {
                    counter++;
                }
                else if (FULL.equals(communicationProtocol)) {
                    System.out.println("lotnisko przepełnione ...");
                    break;
                }
                else if (NOTIME.equals(communicationProtocol)) {
                    System.out.println("paliwa zabrakło  ...");
                    break;
                }
                else {
                    System.out.println("nie wiem ale się dowiem ");
                }
                counter++;



                Thread.sleep(1000);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }


        }
    }

}


