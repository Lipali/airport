package client;

import java.util.Random;

public class Point {
    int x;
    int y;
    int h;

    public Point(int x,int y, int h){
        this.x=x;
        this.y=y;
        this.h=h;
    }
    public Point() {
        Random generator = new Random();
        int temp1 = generator.nextInt(4);
        int temp2 = generator.nextInt(1001);
        int temp3 = generator.nextInt(301) + 200;

        switch (temp1) {
            case 0:
                this.x = 0;
                this.y = temp2;
                this.h = temp3;
                break;
            case 1:
                this.x = 1000;
                this.y = temp2;
                this.h = temp3;
                break;
            case 2:
                this.x = temp2;
                this.y = 0;
                this.h = temp3;
                break;
            case 3:
                this.x = temp2;
                this.y = 1000;
                this.h = temp3;
        }
    }
    public void setX(int xParameter) {
        x = xParameter;
    }
    public int getX() {
        return x;
    }
    public void setY(int yParameter) {
        y= yParameter;
    }
    public int getY() {
        return y;
    }
    public void setH(int hParameter) {
        h = hParameter;
    }
    public int getH() {
        return h;
    }

    public void printPoint(){
        System.out.println(x+y+h);
    }



}
