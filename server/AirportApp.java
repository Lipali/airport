package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class AirportApp {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(5051);
        Socket socket;
        while (true){
            socket=serverSocket.accept();
            AirportThread airportThread = new AirportThread(socket);
            airportThread.start();
        }

    }


}
