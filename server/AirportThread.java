package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import client.Point;
import com.google.gson.Gson;


public class AirportThread extends Thread {
    Socket socket ;
    DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
    DataInputStream din = new DataInputStream(socket.getInputStream());
    PlanesDB planesDB = new PlanesDB();
    PlanesCoordinatesDB planesCoordinatesDB = new PlanesCoordinatesDB();
    PlaneControler planeControler = new PlaneControler();
    CommunicationProtocol communicationProtocol = new CommunicationProtocol();
    Gson gson = new Gson();

    public AirportThread (Socket s) throws IOException {
        this.socket=s;
    }
    LinkedList <String> namesList = new LinkedList<>();
    LinkedList <String> coordList = new LinkedList<>();
    boolean flagName = Boolean.parseBoolean(null);
    boolean flagFly = Boolean.parseBoolean(null);
    @Override
    public void run() {
        while (true){
            try {
                long time =System.currentTimeMillis();
                String planeNameJson = din.readUTF();
                String pointJson = din.readUTF();
                String planeName =gson.fromJson(planeNameJson,String.class);
                Point point = gson.fromJson(pointJson,Point.class);
                namesList= planesDB.selectNames();
                String actualCoord = planesCoordinatesDB.getCoordinate(planeName);
                coordList = planesCoordinatesDB.selectCoordinates();
                coordList = planeControler.canIFlyListMaker(actualCoord,coordList);
                flagName = planeControler.checkNameList(planeName,namesList);
                long timeStart = planesDB.selectTime(planeName);
                if (namesList.size()>=100){
                    communicationProtocol.full();
                }
                else if (flagName==true){
                    communicationProtocol.start();
                }
                else if (planeControler.checkTime(timeStart)==false){
                    communicationProtocol.noTime();
                }
                else if (planeControler.checkCoord(actualCoord,coordList)==true){
                    communicationProtocol.fly();
                }
                else if (planeControler.checkCoord(actualCoord,coordList)==false){
                    communicationProtocol.stop();
                }

                String jsonProtocol = gson.toJson(communicationProtocol);
                dout.writeUTF(jsonProtocol);

            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

}

