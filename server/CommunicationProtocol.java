package server;

public class CommunicationProtocol {
    public enum ValueType {
        STOP , START , FLY , EMPTY , FULL, NOTIME
    }
    public ValueType valueType = ValueType.EMPTY;

    void stop() {
        valueType = ValueType.STOP;
    }
    void full() {
        valueType = ValueType.FULL;
    }
    void start() {
        valueType = ValueType.START;
    }
    void fly() {
        valueType = ValueType.FLY;
    }
    void noTime() {
        valueType = ValueType.NOTIME;
    }
    boolean isFly() {
        return valueType == ValueType.FLY;
    }
    boolean isStart() {
        return valueType == ValueType.START;
    }
    boolean isStop() {
        return valueType == ValueType.STOP;
    }
    boolean isFull() {
        return valueType == ValueType.FULL;
    }
    boolean isNotime() {
        return valueType == ValueType.NOTIME;
    }
}
