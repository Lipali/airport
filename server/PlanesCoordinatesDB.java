package server;

import java.sql.*;
import java.util.LinkedList;

public class PlanesCoordinatesDB {
    private Connection connect() {

        String url = "jdbc:sqlite:C://sqlite/airport2.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    public void insertCoordinates (String name, String coord ) {

        String sql = "INSERT INTO coordinates  (name,coord) VALUES(?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, coord);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public void deleteCoordinates(String name) {
        String sql = "DELETE FROM coordinates WHERE name = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, name);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public void updateCoordinates( String name, String coord) {
        String sql = "UPDATE coordinates SET coord = ? "
                + " WHERE name = ? ";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(2, name);
            pstmt.setString(1, coord);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public String getCoordinate(String name){
        String coord ="";
        String sql = "SELECT coord "
                + "FROM coordinates WHERE name  = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)){

            // set the value
            pstmt.setString(1,name);
            //
            ResultSet rs  = pstmt.executeQuery();

            // loop through the result set
            while (rs.next()) {

                    coord =rs.getString("coord") ;

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return coord;
    }
    public LinkedList<String> selectCoordinates(String name) {
        LinkedList<String> list= new LinkedList<>();
        String sql = "SELECT  coord FROM coordinates where name !=?";

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             stmt.
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                list.add( rs.getString("coord"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }


}
